//import logo from './logo.svg';
import './App.css';
import AppNavbar from "./components/AppNavbar";
import Banner from "./components/Banner";
import Highlights from "./components/Highlights";


function App() {
  return (
    <div className="App">
        <AppNavbar />
        <Banner />
        <Highlights />
    </div>
  );
}



export default App;
